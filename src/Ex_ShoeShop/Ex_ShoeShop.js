import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./DataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[0], //dataShoe[0] la mot object
    cart: [],
  };
  handleChangeDetail = (value) => {
    this.setState({ detail: value }); //truyen vao value nghia la truyen vao 1 object moi
  };
  handleAddtoCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      //th2: sp chua co => tao moi va push
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
      //th1: san pham da co trong cart => tang number
    } else {
      cloneCart[index].number++;
    }
    this.setState({
      cart: cloneCart,
    });

    // if ((cloneCart = [cartItem])) {
    //   cartItem.number++;
    // } else {
    //   cloneCart.push(cartItem);
    // }
  };
  render() {
    return (
      <div className="container">
        <Cart cart={this.state.cart} />
        <ListShoe
          handleAddtoCart={this.handleAddtoCart}
          handleChangeDetail={this.handleChangeDetail}
          shoeArr={this.state.shoeArr}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}

import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { name, alias, price, shortDescription, quantity, image } =
      this.props.detail;
    return (
      <div className="row pt-5">
        <img className="col-4" src={image} alt={alias} />
        <div className="col-8 text-left">
          <h3>{name}</h3>
          <p>Giá sản phẩm: {price}$</p>
          <p>Mô tả: {shortDescription}</p>
          <p>Số lượng còn lại: {quantity} (chiếc)</p>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name } = this.props.data;
    return (
      <div className="col-3">
        <div className="card text-left">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <button
              onClick={() => {
                this.props.handleAddtoCart(this.props.data);
              }}
              className="btn btn-success mr-5 mb-2"
            >
              Add to cart
            </button>
            <button
              onClick={() => {
                this.props.handleViewDetail(this.props.data); //this.props.data tra ve 1 object
              }}
              className="btn btn-primary"
            >
              View details
            </button>
          </div>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import { dataShoe } from "./DataShoe"; //neu da export const thi khi import thi ten bien phai co ngoac nhon {}
import ItemShoe from "./ItemShoe";
export default class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((item) => {
      return (
        <ItemShoe
          handleAddtoCart={this.props.handleAddtoCart}
          handleViewDetail={this.props.handleChangeDetail}
          data={item}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
//Explaining PROPS: We use props in React to pass data from one component to another (from a parent component to a child component)

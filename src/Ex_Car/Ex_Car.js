import React, { Component } from "react";

export default class Ex_Car extends Component {
  state = {
    img: "./img/CarBasic/products/black-car.jpg",
  };
  handleChangeColor = (color) => {
    this.setState({
      img: `./img/CarBasic/products/${color}-car.jpg`,
    });
  };
  render() {
    return (
      <div className="container py-5">
        <div className="row align-items-center">
          <img src={this.state.img} className="col-4" alt="" />
          <div>
            <button
              onClick={() => {
                this.handleChangeColor("black");
              }}
              className="btn btn-dark"
            >
              Black
            </button>
            <button
              onClick={() => {
                this.handleChangeColor("red");
              }}
              className="btn btn-danger mx-2"
            >
              Red
            </button>
            <button
              onClick={() => {
                this.handleChangeColor("silver");
              }}
              className="btn btn-secondary"
            >
              Silver
            </button>
          </div>
        </div>
      </div>
    );
  }
}

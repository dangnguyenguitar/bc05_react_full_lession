import React, { Component } from "react";

export default class CardItem extends Component {
  render() {
    return (
      <div>
        <div className="card text-left">
          <img
            style={{ height: "80%", objectFit: "cover" }}
            className="card-img-top"
            src={this.props.movie.hinhAnh}
            alt
          />
          <div className="card-body">
            <h4 className="card-title">{this.props.movie.tenPhim}</h4>
            <p className="card-text">{this.props.movie.tenPhim}</p>
            <button className="btn btn-warning">Mua ngay</button>
          </div>
        </div>
      </div>
    );
  }
}

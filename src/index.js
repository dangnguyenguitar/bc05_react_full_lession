import React, { StrictMode } from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { rootReducer_Demo_Mini } from "./Demo_Mini_Redux/redux/reducer/rootReducer";
import { rootReducer_Shoe_Shop } from "./Ex_ShoeShop_Redux/Redux/Reducer/rootReducer";
import { rootReducer_XucXac } from "./Ex_Tai_Xiu/redux/rootReducer";
import { createStore } from "redux";

// const store = createStore(
//   rootReducer_XucXac,
//   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
// );

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  // <Provider store={store}>
  // <StrictMode>
  <App />
  // </StrictMode>
  // </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

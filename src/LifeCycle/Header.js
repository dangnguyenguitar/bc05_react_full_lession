import React, { Component, PureComponent } from "react";

export default class extends PureComponent {
  componentDidMount() {
    let time = 300;
    this.myCountDown = setInterval(() => {
      console.log("count down", time--);
    }, 1000);
  }
  render() {
    console.log("Header render");
    return <div className="p-5 bg-dark text-white">Header</div>;
  }
  componentWillUnmount() {
    console.log("HEADER componentWillUnmount");
    clearInterval(this.myCountDown);
  }
}

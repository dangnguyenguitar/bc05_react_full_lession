import React, { Component } from "react";
import Header from "./Header";
export default class LifeCycle extends Component {
  state = {
    like: 1,
  };
  componentDidMount() {
    console.log("Life_cycle componendidmount");
    //chỉ chạy 1 lần duy nhất (sau khi render chạy)
    //thường để gọi api
  }
  handlePlusLike = () => {
    this.setState({ like: this.state.like + 1 });
  };
  shouldComponentUpdate(nextProps, nextState) {
    //mặc định là return true
    if (nextState.like == 3) {
      return false;
    }
    return true;
  }
  render() {
    console.log("life_cycle render");
    return (
      <div className="p-5 bg-primary">
        {this.state.like < 5 && <Header />}
        <p>lifeCycle</p>
        <span className="display-4">{this.state.like}</span>
        <button className="btn btn-success" onClick={this.handlePlusLike}>
          plus like
        </button>
      </div>
    );
  }
  componentDidUpdate() {
    console.log("componentDidUpdate");
  }
}

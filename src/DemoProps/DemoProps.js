import React, { Component } from "react";
import UserInfo from "./UserInfo";

export default class DemoProps extends Component {
  renderTitle = () => {
    return <h1>Nice to meet you</h1>;
  };
  handleClickme = () => {
    console.log("first");
  };
  render() {
    let user = {
      name: "Bob",
      gmail: "bob@gmail.com",
    };
    return (
      <div>
        <h2>DemoProps</h2>
        <button onClick={this.handleClickme} className="btn btn-success">
          Click me
        </button>
        <UserInfo renderContent={this.renderTitle} userData={user} />
      </div>
    );
  }
}

import React, { Component } from "react";

export default class UserInfo extends Component {
  render() {
    console.log(this.props); // props là có sẵn và nằm trong node modules
    return (
      <div>
        <h2>Username:{this.props.userData.name}</h2>
        <h2>Gmail:{this.props.userData.gmail}</h2>
        {this.props.renderContent()}
      </div>
    );
  }
}

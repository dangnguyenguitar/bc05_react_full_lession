import React, { Component } from "react";
import { connect } from "react-redux";
import { dataShoe } from "./DataShoe"; //neu da export const thi khi import thi ten bien phai co ngoac nhon {}
import ItemShoe from "./ItemShoe";
class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.list.map((item) => {
      return <ItemShoe data={item} />;
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
let mapStateToProps = (state) => {
  return {
    list: state.shoeReducer.shoeArr,
  };
};
export default connect(mapStateToProps)(ListShoe);

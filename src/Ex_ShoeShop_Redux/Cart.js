import React, { Component } from "react";
import { connect } from "react-redux";
import ListShoe from "./ListShoe";
class Cart extends Component {
  renderTbody = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>${item.price * item.number}</td>
          <td>{item.number}</td>
          <td>
            <img style={{ width: "80px" }} src={item.image} alt="" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Price</td>
            <td>Quantity</td>
            <td>Image</td>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.cart,
  };
};
export default connect(mapStateToProps)(Cart);

import React, { Component } from "react";

export default class UserForm extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }
  state = {
    user: {
      name: "",
      password: "",
    },
  };
  componentDidMount() {
    // this.inputRef.current.value = "Alice";
    // this.inputRef.current.style.color = "red";
  }
  handleGetUsername = (e) => {
    console.log(e.target.value);
    let value = e.target.value;
    let cloneUser = { ...this.state.user };
    cloneUser.name = value;
    this.setState({ user: cloneUser }, () => {
      console.log(this.state.user);
    });
  };
  render() {
    return (
      <div>
        <div className="form-group">
          <input
            onChange={this.handleGetUsername}
            value={this.state.user.name}
            ref={this.inputRef}
            type="text"
            className="form-control"
            placeholder="Username"
          />
        </div>
        <div className="form-group">
          <input
            onChange={this.handleGetUsername}
            value={this.state.user.password}
            type="text"
            className="form-control"
            placeholder="Password"
          />
        </div>
        <button type="button" className="btn btn-warning">
          Add user
        </button>
      </div>
    );
  }
}

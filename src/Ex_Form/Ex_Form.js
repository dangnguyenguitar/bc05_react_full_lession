import React, { Component } from "react";
import UserForm from "./UserForm";
import UserTable from "./UserTable";

export default class Ex_Form extends Component {
  render() {
    return (
      <div className="container py-5">
        <UserForm />
        <UserTable />
      </div>
    );
  }
}

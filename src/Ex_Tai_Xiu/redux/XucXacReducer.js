import { CHOOSE_OPTION, PLAY_GAME } from "./xucXacConstant";

let initialState = {
  mangXucXac: [
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
  ],
  luaChon: null,
};
export const xucXacReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case PLAY_GAME: {
      //random from ... to ... js
      let newMangXucXac = state.mangXucXac.map((item) => {
        let random = Math.floor(Math.random() * 6) + 1;
        return {
          img: `./imgXucSac/${random}.png`,
          giaTri: random,
        };
      });
      state.mangXucXac = newMangXucXac;
      return { ...state };
    }
    case CHOOSE_OPTION: {
      console.log(payload);
    }
    default:
      return state;
  }
};
